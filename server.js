
const express = require("express");
const bodyParser = require("body-parser");
const sqlite3 = require("sqlite3").verbose();
const path = require("path");
const app = express();
const port = 3000;


app.use(bodyParser.json());


app.use(express.static("public"));

const db = new sqlite3.Database(":memory:", (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log("Connected to the in-memory SQLite database.");
});


const initializeDatabase = () => {
    db.run(
        "CREATE TABLE IF NOT EXISTS drugs (id TEXT PRIMARY KEY, name TEXT, quantity INTEGER, requiresPrescription INTEGER);",
    );
    db.run(
        "CREATE TABLE IF NOT EXISTS orders (id TEXT PRIMARY KEY, customerName TEXT, orderDate TEXT, prescription TEXT, drugId TEXT, FOREIGN KEY(drugId) REFERENCES drugs(id));",
    );
    db.run(
        "CREATE TABLE IF NOT EXISTS order_drugs (drugOrderId INTEGER PRIMARY KEY AUTOINCREMENT, orderId TEXT, drugId TEXT, quantity INTEGER, FOREIGN KEY(orderId) REFERENCES orders(id), FOREIGN KEY(drugId) REFERENCES drugs(id));"
    );
};


const deletePastOrders = () => {
    const today = new Date();
    // today.setHours(0, 0, 0, 0);

    db.run(
        "DELETE FROM orders WHERE orderDate < ?",
        today.toISOString(),
        (err) => {
            if (err) {
                console.error("Error deleting past orders: " + err.message);
                return;
            }
            console.log("Past orders deleted.");
        },
    );
};

initializeDatabase();
deletePastOrders();


const isDateInPast = (dateString) => {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    const inputDate = new Date(dateString);
    console.log("checking 1" + inputDate);
    console.log("checking 2" + today);
    return inputDate < today;
};


app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "public", "index.html"));
});


app.post("/orders", (req, res) => {
    const { id, customerName, orderDate, prescription, drugId } = req.body;
    if (isDateInPast(orderDate)) {
        return res.status(400).json({ error: "Order date cannot be in the past." });
    }
    console.log(orderDate);
    db.run(
        "INSERT INTO orders (id, customerName, orderDate, prescription, drugId) VALUES (?, ?, ?, ?, ?)",
        [id, customerName, orderDate, prescription, drugId],
        (err) => {
            if (err) {
                res.status(400).json({ error: err.message });
                return;
            }
            res.json({ message: "success", data: req.body });
        },
    );
});


app.get("/orders", (req, res) => {
    db.all("SELECT orders.*, drugs.name as drugName FROM orders LEFT JOIN drugs ON orders.drugId = drugs.id", [], (err, rows) => {
        if (err) {
            res.status(400).json({ error: err.message });
            return;
        }
        res.json({ message: "success", data: rows });
    });
});





app.put("/orders/:id", (req, res) => {
    const id = req.params.id;
    const { customerName, orderDate, prescription, drugId } = req.body;
    if (isDateInPast(orderDate)) {
        return res.status(400).json({ error: "Order date cannot be in the past." });
    }
    db.run(
        "UPDATE orders SET customerName = ?, orderDate = ?, prescription = ?, drugId = ? WHERE id = ?",
        [customerName, orderDate, prescription, drugId, id],
        (err) => {
            if (err) {
                res.status(400).json({ error: err.message });
                return;
            }
            res.json({ message: "success", data: req.body });
        },
    );
});


app.delete("/orders/:id", (req, res) => {
    db.run("DELETE FROM orders WHERE id = ?", req.params.id, (err) => {
        if (err) {
            res.status(400).json({ error: err.message });
            return;
        }
        res.json({ message: "success" });
    });
});




app.post("/drugs", (req, res) => {
    const { id, name, quantity, requiresPrescription } = req.body;
    db.run(
        "INSERT INTO drugs (id, name, quantity, requiresPrescription) VALUES (?, ?, ?, ?)",
        [id, name, quantity, requiresPrescription],
        (err) => {
            if (err) {
                res.status(400).json({ error: err.message });
                return;
            }
            res.json({ message: "success", data: req.body });
        },
    );
});

// GET a specific drug by ID
app.get("/drugs/:id", (req, res) => {
    const id = req.params.id;
    db.get("SELECT * FROM drugs WHERE id = ?", [id], (err, row) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        if (row) {
            res.json({ message: "success", data: row });
        } else {
            res.status(404).json({ error: "Drug not found" });
        }
    });
});

app.get("/drugs", (req, res) => {
    db.all("SELECT * FROM drugs", [], (err, rows) => {
        if (err) {
            res.status(400).json({ error: err.message });
            return;
        }
        res.json({ message: "success", data: rows });
    });
});




app.put("/drugs/:id", (req, res) => {
    const id = req.params.id;
    const { name, quantity, requiresPrescription } = req.body;
    db.run(
        "UPDATE drugs SET name = ?, quantity = ?, requiresPrescription = ? WHERE id = ?",
        [name, quantity, requiresPrescription, id],
        (err) => {
            if (err) {
                res.status(400).json({ error: err.message });
                return;
            }
            res.json({ message: "success", data: req.body });
        },
    );
});


app.delete("/drugs/:id", (req, res) => {
    db.run("DELETE FROM drugs WHERE id = ?", req.params.id, (err) => {
        if (err) {
            res.status(400).json({ error: err.message });
            return;
        }
        res.json({ message: "success" });
    });
});


app.post("/order-drugs", (req, res) => {
    const { orderId, drugId, quantity } = req.body;


    db.get(
        "SELECT quantity, requiresPrescription FROM drugs WHERE id = ?",
        [drugId],
        (err, drug) => {
            if (err) {
                res.status(500).json({ error: err.message });
                return;
            }
            if (!drug || drug.quantity < quantity) {
                res
                    .status(400)
                    .json({ error: "Drug not available in required quantity." });
                return;
            }
            if (drug.requiresPrescription) {

                db.get(
                    "SELECT prescription FROM orders WHERE id = ?",
                    [orderId],
                    (err, order) => {
                        if (err) {
                            res.status(500).json({ error: err.message });
                            return;
                        }
                        if (!order || !order.prescription) {
                            res
                                .status(400)
                                .json({ error: "Prescription required for this drug." });
                            return;
                        }
                        addDrugToOrder();
                    },
                );
            } else {
                addDrugToOrder();
            }
        },
    );

    function addDrugToOrder() {
        db.run(
            "INSERT INTO order_drugs (orderId, drugId, quantity) VALUES (?, ?, ?)",
            [orderId, drugId, quantity],
            (err) => {
                if (err) {
                    res.status(500).json({ error: err.message });
                    return;
                }
                res.json({ message: "Drug added to order successfully." });
            },
        );
    }
});


const randomIncrease = () => {

    return Math.floor(Math.random() * 10) + 1;
};


app.post("/simulate-day-forward", (req, res) => {
    const today = new Date();
    // today.setHours(0, 0, 0, 0);
    db.run(
        "DELETE FROM orders WHERE orderDate = ?",
        [today.toISOString().split('T')[0]],
        (err) => {
            if (err) {
                return res.status(500).json({ error: err.message });
            }


            db.all("SELECT id FROM drugs", [], (err, rows) => {
                if (err) {
                    return res.status(500).json({ error: err.message });
                }


                const updatePromises = rows.map((drug) => {
                    return new Promise((resolve, reject) => {
                        db.run(
                            "UPDATE drugs SET quantity = quantity + ? WHERE id = ?",
                            [randomIncrease(), drug.id],
                            (err) => {
                                if (err) reject(err);
                                resolve();
                            },
                        );
                    });
                });

                Promise.all(updatePromises)
                    .then(() => {
                        res.json({
                            message:
                                "Day forwarded, orders updated, and drug stocks replenished.",
                        });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: error.message });
                    });
            });
        },
    );
});


// Modify POST Endpoint to include drugOrderId
app.post("/orders/:orderId/drugs", (req, res) => {
    const { orderId } = req.params;
    const { drugId, quantity } = req.body;

    db.get("SELECT * FROM orders WHERE id = ?", [orderId], (orderErr, order) => {
        if (orderErr || !order) {
            res.status(404).json({ error: "Order not found" });
            return;
        }

        db.get("SELECT * FROM drugs WHERE id = ?", [drugId], (drugErr, drug) => {
            if (drugErr || !drug || drug.quantity < quantity) {
                res.status(404).json({ error: "Drug not found or insufficient quantity" });
                return;
            }

            db.run(
                "INSERT INTO order_drugs (orderId, drugId, quantity) VALUES (?, ?, ?)",
                [orderId, drugId, quantity],
                (insertErr) => {
                    if (insertErr) {
                        res.status(500).json({ error: insertErr.message });
                        return;
                    }

                    // Update the quantity of the drug in the drugs table
                    const updatedQuantity = drug.quantity - quantity;
                    db.run("UPDATE drugs SET quantity = ? WHERE id = ?", [updatedQuantity, drugId], (updateErr) => {
                        if (updateErr) {
                            res.status(500).json({ error: updateErr.message });
                            return;
                        }

                        res.json({ message: "Drug added to order successfully" });
                    });
                }
            );
        });
    });
});

// Modify GET and DELETE Endpoints to work with drugOrderId
app.get("/orders/:orderId/drugs", (req, res) => {
    const orderId = req.params.orderId;


    db.all(
        "SELECT od.drugOrderId, d.id, d.name, od.quantity, d.requiresPrescription FROM order_drugs od JOIN drugs d ON od.drugId = d.id WHERE od.orderId = ?",
        [orderId],
        (err, drugs) => {
            if (err) {
                res.status(500).json({ error: err.message });
                return;
            }

            res.json({ message: "success", data: drugs });
        }
    );
});

app.delete("/orders/:orderId/drugs/:drugOrderId", (req, res) => {
    const { orderId, drugOrderId } = req.params;

    db.run(
        "DELETE FROM order_drugs WHERE orderId = ? AND drugOrderId = ?",
        [orderId, drugOrderId],
        (deleteErr) => {
            if (deleteErr) {
                res.status(500).json({ error: deleteErr.message });
                return;
            }

            res.json({ message: "Drug removed from order successfully" });
        }
    );
});

// Update Transfer Endpoint to work with drugOrderId
app.put("/transfer-drug/:fromOrderId/:toOrderId/:drugOrderId", (req, res) => {
    const { fromOrderId, toOrderId, drugOrderId } = req.params;

    db.serialize(() => {
        db.run("BEGIN TRANSACTION;");

        db.run(
            "UPDATE order_drugs SET orderId = ? WHERE orderId = ? AND drugOrderId = ?",
            [toOrderId, fromOrderId, drugOrderId],
            (updateErr) => {
                if (updateErr) {
                    db.run("ROLLBACK;");
                    res.status(500).json({ error: updateErr.message });
                    return;
                }

                db.run("COMMIT;");
                res.json({ message: "Drug transferred successfully." });
            }
        );
    });
});




app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
