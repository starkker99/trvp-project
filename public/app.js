// Global variables to store currentOrderId and drugId
let globalCurrentOrderId;
let globalDrugId;

document.addEventListener("DOMContentLoaded", () => {
    getOrders();
    populateDrugDropdown();
    populateDrugDropdown2();
    populateEditDrugDropdown();

    const currentOrderId = globalCurrentOrderId;
    const drugId = globalDrugId;

    populateDestinationOrderDropdown(currentOrderId, drugId);

    document.getElementById("next-day-button").addEventListener("click", function () {
        simulateDayForward();
    });
    document
        .getElementById("add-order-form")
        .addEventListener("submit", handleAddOrder);
    document.getElementById("add-drug-form").addEventListener("submit", function (event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        const drugData = Object.fromEntries(formData.entries());

        const orderId = document.getElementById("current-order-id").textContent;

        if (orderId) {
            addDrug(drugData, orderId);
        } else {
            console.error("No order selected for adding drugs.");
        }
    });

    document
        .getElementById("edit-order-form")
        .addEventListener("submit", function (event) {
            event.preventDefault();
            const formData = new FormData(event.target);
            const updatedOrderData = Object.fromEntries(formData.entries());

            fetch(`http://localhost:3000/orders/${updatedOrderData.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(updatedOrderData),
            })
                .then((response) => response.json())
                .then((data) => {
                    const message = data.message ? data.message : JSON.stringify(data);
                    showNotification("Order updated:" + message);
                    getOrders(); // Refresh orders list
                    document
                        .getElementById("edit-order-modal-overlay")
                        .classList.add("hidden");
                })
                .catch((error) => {
                    console.error("Error:", error);
                });
        });


    document.getElementById("add-drug-button").addEventListener("click", function () {
        document.getElementById("add-drug-modal-overlay").classList.remove("hidden");
    });


    document.getElementById("add-new-drug-form").addEventListener("submit", function (event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        const newDrugData = Object.fromEntries(formData.entries());

        fetch("http://localhost:3000/drugs", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newDrugData),
        })
            .then((response) => response.json())
            .then((data) => {
                const message = data.message ? data.message : JSON.stringify(data);
                showNotification("New drug added:" + message);

                document.getElementById("add-drug-modal-overlay").classList.add("hidden");
            })
            .catch((error) => {
                console.error("Error:", error);
            });
    });


    document.getElementById("show-drugs-list-button").addEventListener("click", function () {
        fetchDrugsList();
        document.getElementById("drugs-list-modal-overlay").classList.remove("hidden");
    });

});

function populateDrugDropdown2() {
    fetch("http://localhost:3000/drugs")
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("drugsDropdown");
            data.data.forEach(drug => {
                const option = document.createElement("option");
                option.value = drug.id;
                option.textContent = drug.name;
                dropdown.appendChild(option);
            });
        })
        .catch(error => console.error("Error:", error));
}

function populateDrugDropdown() {
    fetch("http://localhost:3000/drugs")
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("drugDropdown");
            data.data.forEach(drug => {
                const option = document.createElement("option");
                option.value = drug.id;
                option.textContent = drug.name;
                dropdown.appendChild(option);
            });
        })
        .catch(error => console.error("Error:", error));
}

function populateEditDrugDropdown() {
    fetch("http://localhost:3000/drugs")
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("editDrugDropdown");
            data.data.forEach(drug => {
                const option = document.createElement("option");
                option.value = drug.id;
                option.textContent = drug.name;
                dropdown.appendChild(option);
            });
        })
        .catch(error => console.error("Error:", error));
}

function simulateDayForward() {
    fetch("http://localhost:3000/simulate-day-forward", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(response => response.json())
        .then(data => {
            const message = data.message ? data.message : JSON.stringify(data);
            showNotification("Simulation successful: " + message);

            getOrders();

        })
        .catch(error => {
            console.error("Error simulating next day:", error);
        });
}

function fetchDrugsList() {
    fetch("http://localhost:3000/drugs")
        .then((response) => response.json())
        .then((data) => {
            populateDrugsList(data.data);
        })
        .catch((error) => console.error("Error:", error));
}


function closeUpdateDrugModal() {
    document.getElementById("update-drug-modal-overlay").classList.add("hidden");
}

function deleteDrug(id) {
    if (confirm("Are you sure you want to delete this drug?")) {
        fetch(`http://localhost:3000/drugs/${id}`, {
            method: "DELETE",
        })
            .then((response) => response.json())
            .then((data) => {
                const message = data.message ? data.message : JSON.stringify(data);
                showNotification("Drug deleted:" + message);
                fetchDrugsList();
            })
            .catch((error) => {
                console.error("Error:", error);
            });
    }
}





function handleAddOrder(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const orderData = Object.fromEntries(formData.entries());

    // Add logic to handle prescription requirement if the selected drug requires it
    // This is an example. Adjust based on your actual requirements and data structure

    fetch(`http://localhost:3000/drugs/${orderData.drugId}`)
        .then(response => response.json())
        .then(drugData => {
            if (drugData.data.requiresPrescription && !orderData.prescription) {
                alert("Prescription required for this drug.");
                return; // Prevent the order from being added
            }
            console.log(orderData);
            addOrder(orderData); // Proceed to add the order
        })
        .catch(error => console.error("Error:", error));
}


function handleAddDrug(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const drugData = Object.fromEntries(formData.entries());
    addDrug(drugData);
}

function addOrder(orderData) {
    fetch("http://localhost:3000/orders", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(orderData),
    })
        .then((response) => response.json())
        .then((data) => {
            const message = data.message ? data.message : JSON.stringify(data);
            showNotification("Order added:" + message);
            getOrders();
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}

function addDrug(drugData, orderId) {
    fetch(`http://localhost:3000/orders/${orderId}/drugs`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(drugData),
    })
        .then((response) => response.json())
        .then((data) => {
            const message = data.message ? data.message : JSON.stringify(data);
            showNotification("Drug added to order:" + message);
            viewOrderDrugs(orderId);
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}


let ordersData = [];

function getOrders() {
    fetch("http://localhost:3000/orders")
        .then((response) => response.json())
        .then((data) => {
            ordersData = data.data;
            populateOrdersTable(data.data);
        })
        .catch((error) => console.error("Error:", error));
}



function populateOrdersTable(orders) {
    const tableContainer = document.getElementById("orders-table");
    let tableHTML = '<table class="min-w-full divide-y divide-gray-200">';


    tableHTML += `
        <thead class="bg-gray-50">
            <tr>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Order ID</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Customer Name</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Order Date</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Prescription</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Drug</th>
            </tr>
        </thead>`;


    tableHTML += '<tbody class="bg-white divide-y divide-gray-200">';
    console.log(orders);
    orders.forEach((order) => {
        tableHTML += `
            <tr>
                <td class="px-6 py-4 whitespace-nowrap">${order.id}</td>
                <td class="px-6 py-4 whitespace-nowrap">${order.customerName}</td>
                <td class="px-6 py-4 whitespace-nowrap">${order.orderDate}</td>
                <td class="px-6 py-4 whitespace-nowrap">${order.prescription}</td>
                <td class="px-6 py-4 whitespace-nowrap">${order.drugName}</td>
                <td class="px-6 py-4 whitespace-nowrap">
                <button onclick="editOrder('${order.id}')" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Edit</button>
                <button onclick="deleteOrder('${order.id}')" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">Delete</button>
                <button onclick="viewOrderDrugs('${order.id}')" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded">Open</button>
              </td>
            </tr>`;
    });
    tableHTML += "</tbody></table>";

    tableContainer.innerHTML = tableHTML;
}

function submitTransfer() {
    // Retrieve the currentOrderId and drugId from the modal dataset
    const currentOrderId = globalCurrentOrderId;
    const drugId = globalDrugId;

    const destinationOrderId = document.getElementById("destinationOrder").value;

    // Send a PUT request to the server to initiate the drug transfer
    fetch(`http://localhost:3000/transfer-drug/${currentOrderId}/${destinationOrderId}/${drugId}`, {
        method: "PUT",
    })
        .then(response => response.json())
        .then(data => {
            // Handle the response from the server
            if (data.message) {
                showNotification(data.message);

                // Optionally, you may want to refresh the current order's drugs table after the transfer
                viewOrderDrugs(currentOrderId);
            } else {
                showNotification("Failed to transfer drug.", true);
            }
        })
        .catch(error => {
            console.error("Error:", error);
            showNotification("Failed to transfer drug. Please try again later.", true);
        });

    // Close the transfer modal after submission
    closeTransferModal();
}


function populateDestinationOrderDropdown(currentOrderId, drugId) {
    // Fetch the list of orders and populate the destinationOrder dropdown
    fetch("http://localhost:3000/orders")
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("destinationOrder");
            dropdown.innerHTML = ""; // Clear existing options

            data.data.forEach(order => {
                if (!currentOrderId || order.id !== currentOrderId) {
                    const option = document.createElement("option");
                    option.value = order.id;
                    option.textContent = `Order: ${order.id} - Customer Name: ${order.customerName}`;
                    dropdown.appendChild(option);
                }
            });
        })
        .catch(error => console.error("Error:", error));

    // Store the currentOrderId and drugId in the modal for later use
    const transferModal = document.getElementById("transferModal");
    transferModal.dataset.currentOrderId = currentOrderId;
    transferModal.dataset.drugId = drugId;
    console.log(currentOrderId);
    console.log(drugId);
}


function viewOrderDrugs(orderId) {
    fetch(`http://localhost:3000/orders/${orderId}/drugs`)
        .then((response) => response.json())
        .then((data) => {
            if (data.message === "success") {
                // Update current order ID
                document.getElementById("current-order-id").textContent = orderId;
                // Populate drugs table
                populateDrugsTable(data.data, orderId); // Pass orderId as a parameter
                // Show the order-drugs-section
            } else {
                console.error("Failed to fetch drugs for order: ", orderId);
            }
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}

function closeAddDrugModal() {
    document.getElementById("add-drug-modal-overlay").classList.add("hidden");
}


function closeDrugsListModal() {
    document.getElementById("drugs-list-modal-overlay").classList.add("hidden");
}

function showNotification(message, isError = false) {
    const container = document.getElementById("notification-container");
    const notification = document.createElement("div");

    notification.className = `notification ${isError ? 'error' : ''}`;
    notification.textContent = message;

    container.appendChild(notification);
    container.classList.add('show');

    setTimeout(() => {
        notification.classList.add('hide');
        setTimeout(() => {
            notification.remove();
            if (!container.hasChildNodes()) {
                container.classList.remove('show'); // Hide the container if empty
            }
        }, 300);
    }, 5000);
}

function deleteDrugFromOrder(orderId, drugId) {
    if (confirm("Are you sure you want to delete this drug from the order?")) {
        fetch(`/orders/${orderId}/drugs/${drugId}`, {
            method: 'DELETE',
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                console.log(data.message);
                // Update the UI or perform other actions as needed
                // For example, you may want to refresh the list of drugs in the order
                viewOrderDrugs(orderId);
            })
            .catch(error => {
                console.error('Error:', error);
                // Handle errors, show an error message, or perform other actions as needed
            });
    }
}


function populateDrugsTable(drugs, orderId) {
    const tableContainer = document.getElementById("drugs-table");
    let tableHTML = '<table class="min-w-full divide-y divide-gray-200">';

    tableHTML += `
        <thead class="bg-gray-50">
            <tr>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Drug ID</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Quantity</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Requires Prescription</th>
                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Actions</th>
            </tr>
        </thead>`;

    tableHTML += '<tbody class="bg-white divide-y divide-gray-200">';
    drugs.forEach((drug) => {
        tableHTML += `
            <tr>
                <td class="px-6 py-4 whitespace-nowrap">${drug.id}</td>
                <td class="px-6 py-4 whitespace-nowrap">${drug.name}</td>
                <td class="px-6 py-4 whitespace-nowrap">${drug.quantity}</td>
                <td class="px-6 py-4 whitespace-nowrap">${drug.requiresPrescription ? "Yes" : "No"}</td>
                <td class="px-6 py-4 whitespace-nowrap">
                    <button onclick="deleteDrugFromOrder('${orderId}', '${drug.id}')" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline ml-2">Delete</button>
                    <button onclick="openTransferModal('${orderId}', '${drug.drugOrderId}')" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline ml-2">Transfer</button>
                </td>
                
            </tr>`;
    });
    tableHTML += "</tbody></table>";

    tableContainer.innerHTML = tableHTML;
}

function openTransferModal(orderId, drugId) {
    // Store the values in global variables
    globalCurrentOrderId = orderId;
    globalDrugId = drugId;
    // Show the transfer modal
    document
        .getElementById("transfer-drug-modal-overlay")
        .classList.remove("hidden");
    // populateDestinationOrderDropdown(orderId, drugId);
}

function closeTransferModal() {
    document
        .getElementById("transfer-drug-modal-overlay")
        .classList.add("hidden");
}

function populateDrugsList(drugs) {
    let tableHTML = `<table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Drug ID</th>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Quantity</th>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Requires Prescription</th>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">`;

    drugs.forEach((drug) => {
        tableHTML += `<tr>
                        <td class="px-6 py-4 whitespace-nowrap">${drug.id}</td>
                        <td class="px-6 py-4 whitespace-nowrap">${drug.name}</td>
                        <td class="px-6 py-4 whitespace-nowrap">${drug.quantity}</td>
                        <td class="px-6 py-4 whitespace-nowrap">${drug.requiresPrescription ? "Yes" : "No"}</td>
                        <td class="px-6 py-4 whitespace-nowrap text-right">
                            <button onclick="deleteDrug('${drug.id}')" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline ml-2">Delete</button>
                        </td>
                      </tr>`;
    });

    tableHTML += `</tbody></table>`;
    document.getElementById("drugs-list-container").innerHTML = tableHTML;
}



function deleteOrder(orderId) {

    fetch(`http://localhost:3000/orders/${orderId}`, {
        method: "DELETE",
    })
        .then(() => {
            getOrders();
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}

function editOrder(orderId) {

    const order = ordersData.find((o) => o.id === orderId);


    const editForm = document.getElementById("edit-order-form");
    editForm.elements.id.value = order.id;
    editForm.elements.customerName.value = order.customerName;
    editForm.elements.orderDate.value = order.orderDate;
    editForm.elements.prescription.value = order.prescription;


    document
        .getElementById("edit-order-modal-overlay")
        .classList.remove("hidden");
}